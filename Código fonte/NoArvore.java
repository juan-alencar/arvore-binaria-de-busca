
public class NoArvore {
	Valor		valor;
	NoArvore	direita;
	NoArvore	esquerda;
	
    public NoArvore(){
        this.valor = null;
        this.direita = null;
        this.esquerda = null;
    }
    public NoArvore(Valor novovalor) {
        this.valor = novovalor;
        this.direita = null;
        this.esquerda = null;
    }
    
	public NoArvore busca(NoArvore no, int valorprocurado) {
		if(no == null)
			return null;
		
		if(no.valor.rgm > valorprocurado)
			return busca(no.esquerda, valorprocurado);
		else if(no.valor.rgm < valorprocurado)
			return busca(no.direita, valorprocurado);
		else
			return no;
	}
	
	public NoArvore insere(NoArvore no, Valor novovalor) {
		if(no == null) {
			no = new NoArvore();
			no.valor = novovalor;
			no.esquerda = no.direita = null;
		} else if (no.valor.rgm > novovalor.rgm) {
			no.esquerda = insere(no.esquerda, novovalor);
		} else if(no.valor.rgm < novovalor.rgm) {
			no.direita = insere(no.direita, novovalor);
		}
		return no;
	}
	
	public void imprimeIn(NoArvore raiz) {
        
		if (raiz != null) {
            imprimeIn(raiz.esquerda);
            System.out.println(raiz.valor.rgm + " - " + raiz.valor.nome);
            imprimeIn(raiz.direita);
        }
    }
	public void imprimePre(NoArvore raiz) {
	        
			if (raiz != null) {
	            System.out.println(raiz.valor.rgm + " - " + raiz.valor.nome);
	            imprimePre(raiz.esquerda);
	            imprimePre(raiz.direita);
	        }
	    }
	public void imprimePos(NoArvore raiz) {
	    
		if (raiz != null) {
			imprimePos(raiz.esquerda);
	        imprimePos(raiz.direita);
	        System.out.println(raiz.valor.rgm + " - " + raiz.valor.nome);
	    }
	}
	
	
	public void esvaziar(NoArvore raiz) {
		if (raiz != null) {
			esvaziar(raiz.esquerda);
			esvaziar(raiz.direita);
			raiz.esquerda = raiz.direita = null;
		}
	}
	
	
	public NoArvore remove(NoArvore raiz, int valoraremover) {
		// faz a busca pelo valor a ser removido
		if (raiz == null) {
//			System.out.println("RGM n�o encontrado.\n");
			return null;
		} else if (raiz.valor.rgm > valoraremover)
			raiz.esquerda = remove(raiz.esquerda, valoraremover);
		else if (raiz.valor.rgm < valoraremover)
			raiz.direita = remove(raiz.direita, valoraremover);
		else {	// passar por aqui significa que achou o n� com o
				// valor a remover procurado e agora vai remov�-lo
				// segundo as 4 situa��es a seguir:
			// N�o ter filhos (esquerda e direita == null)
			if (raiz.esquerda == null && raiz.direita == null) {
				raiz = null;
			}
			// Ter filho apenas � esquerda (esquerda == null)
			else if (raiz.direita == null) {
				raiz = raiz.esquerda;
			}
			// Ter filho apenas � direita (direita == null)
			else if (raiz.esquerda == null) {
				raiz = raiz.direita;
			}
			// Ter dois filhos (esquerda e direita != null)
			else {
				NoArvore sub_direita = raiz.direita;
				// encontrar o n� com menor valor na sub�rvore direita
				while (sub_direita.esquerda != null) {
					sub_direita = sub_direita.esquerda;
				}
				// aqui a sub_direita.valor tem o menor valor
				raiz.valor = sub_direita.valor;
				sub_direita.valor.rgm = valoraremover;
				raiz.direita = remove(raiz.direita, valoraremover);
			}
		}
		return raiz;		
	}
}